# Table Detection project

Проект по детекции таблицы по изображению и расчету количества строк и столбцов.
Решение выполнено в виде скрипта, принимающего на вход один параметр - имя файла-изображения.

## Установка
1. Склонировать репозиторий проекта:

`$ git clone git@gitlab.com:main_projects1/table_detection.git`

2. Установить необходимые Python-библиотеки:

```
$ pip install opencv-python
$ pip install torch
$ pip install torchvision
$ pip install -U layoutparser
$ pip install 'git+https://github.com/facebookresearch/detectron2.git@v0.4#egg=detectron2'
```

## Запуск
Запустить скрипт детекции таблицы по изображению и расчету количества строк и столбцов:

`$ python detect.py --img-path /path/to/img`

Описание параметров запуска доступно по команде:

`$ python detect.py -h`
```console
usage: detect.py [-h] --img-path /path/to/img

Запуск процедуры детекции таблицы и расчета количество строк и столбцов

options:
  -h, --help            show this help message and exit
  --img-path /path/to/img
                        Путь к img файлу
```
В результате работы скрипта в консоли на экран выводится через пробел два значения: число строк  и число колонок обнаруженной таблицы.
Если таблицы на изображении нет, выводится: 0 0.
