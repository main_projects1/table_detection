"""

Detection of table with calculation the number of rows and columns.

------------------------------------------------------------

Usage: run from the command line:

    #Build detection
    python detect.py --img-path /path/to/img

"""

import argparse
import layoutparser as lp
from pathlib import Path
import cv2
import numpy as np
import os

try:
    from PIL import Image
except ImportError:
    import Image


def exist_table(src_img) -> bool:
    image = cv2.imread(src_img)
    image = image[..., ::-1]

    model = lp.Detectron2LayoutModel('lp://TableBank/faster_rcnn_R_50_FPN_3x/config',
                                     extra_config=["MODEL.ROI_HEADS.SCORE_THRESH_TEST", 0.81],
                                     label_map={0: "Table"})
    layout = model.detect(image)  # You need to load the image somewhere else, e.g., image = cv2.imread(...)

    text_blocks = lp.Layout([b for b in layout])

    if text_blocks:
        return True
    else:
        return False


def count_rows_columns(src_img):
    # read your file
    img = cv2.imread(src_img, 0)

    # thresholding the image to a binary image
    thresh, img_bin = cv2.threshold(img, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

    # inverting the image
    img_bin = 255 - img_bin

    # countcol(width) of kernel as 100th of total width
    kernel_len = np.array(img).shape[1] // 100
    # Defining a vertical kernel to detect all vertical lines of image
    ver_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, kernel_len))
    # Defining a horizontal kernel to detect all horizontal lines of image
    hor_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_len, 1))
    # A kernel of 2x2
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 2))

    # Use vertical kernel to detect and save the vertical lines in a jpg
    image_1 = cv2.erode(img_bin, ver_kernel, iterations=3)
    vertical_lines = cv2.dilate(image_1, ver_kernel, iterations=3)

    # Use horizontal kernel to detect and save the horizontal lines in a jpg
    image_2 = cv2.erode(img_bin, hor_kernel, iterations=3)
    horizontal_lines = cv2.dilate(image_2, hor_kernel, iterations=3)

    # Combine horizontal and vertical lines in a new third image, with both having same weight.
    img_vh = cv2.addWeighted(vertical_lines, 0.5, horizontal_lines, 0.5, 0.0)
    # Eroding and thresholding the image
    img_vh = cv2.erode(~img_vh, kernel, iterations=2)
    thresh, img_vh = cv2.threshold(img_vh, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

    # Detect contours for following box detection
    contours, hierarchy = cv2.findContours(img_vh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # Sort all the contours by top to bottom.
    contours, boundingBoxes = sort_contours(contours, method="top-to-bottom")

    # Creating a list of heights for all detected boxes
    heights = [boundingBoxes[i][3] for i in range(len(boundingBoxes))]

    # Get mean of heights
    mean = np.mean(heights)

    # Create list box to store all boxes in
    box = []
    # Get position (x,y), width and height for every contour and show the contour on image
    for c in contours:
        x, y, w, h = cv2.boundingRect(c)
        if (w < 1000 and h < 500):
            box.append([x, y, w, h])

    # Creating two lists to define row and column in which cell is located
    rows, columns = [], []

    # Sorting the boxes to their respective row and column
    for i, bb in enumerate(box):
        if i == 0:
            columns.append(bb)
            previous = bb
        else:
            if bb[1] < previous[1] + previous[3] / 2:
                columns.append(bb)
                previous = bb
                if i == len(box) - 1:
                    rows.append(columns)
            else:
                rows.append(columns)
                columns = []
                previous = bb
                columns.append(bb)

    return len(rows), len(columns)


def sort_contours(cnts, method="left-to-right"):
    # initialize the reverse flag and sort index
    reverse = False
    i = 0
    # handle if we need to sort in reverse
    if method == "right-to-left" or method == "bottom-to-top":
        reverse = True
    # handle if we are sorting against the y-coordinate rather than
    # the x-coordinate of the bounding box
    if method == "top-to-bottom" or method == "bottom-to-top":
        i = 1
    # construct the list of bounding boxes and sort them from top to
    # bottom
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
                                        key=lambda b: b[1][i], reverse=reverse))
    # return the list of sorted contours and bounding boxes
    return (cnts, boundingBoxes)


if __name__ == '__main__':
    # Parse command line arguments
    parser = argparse.ArgumentParser(description='Запуск процедуры детекции таблицы и расчета количество строк и столбцов')
    parser.add_argument('--img-path', required=True,
                        metavar='/path/to/img', help='Путь к img файлу')
    args = parser.parse_args()

    # Validate arguments
    if not os.path.exists(Path(args.img_path)):
        exit("Provide correct --img-path to use detection")

    # Check existence of table on img
    if not exist_table(args.img_path):
        print('0 0')
        exit()

    # Calculate the number of rows and columns
    rows, columns = count_rows_columns(args.img_path)
    print(rows, columns)
